class Penguin():
    def __init__(self, blocks_boundaries, penguin_image, object_size, speed, wall_size, game_board_y):
        self.x = blocks_boundaries[6][6][0][0]
        self.y = blocks_boundaries[6][6][0][1]
        self.penguin_image = penguin_image
        self.object_size = object_size
        self.speed = speed
        self.in_grid = True
        self.off_grid_movement_direction = None
        self.wall_size = wall_size
        self.game_board_y = game_board_y
    def draw_penguin(self):
        image(self.penguin_image, self.x, self.y, self.object_size, self.object_size)
    def move_right(self):
        self.x += self.speed
        fill(0)
        rect(self.x - self.speed, self.y, self.object_size, self.object_size)
        self.draw_penguin()
        self.off_grid_movement_direction = "r"
    def move_left(self):
        self.x -= self.speed
        fill(0)
        rect(self.x + self.speed, self.y, self.object_size, self.object_size)
        self.draw_penguin()
        self.off_grid_movement_direction = "l"
    def move_up(self):
        self.y -= self.speed
        fill(0)
        rect(self.x, self.y + self.speed, self.object_size, self.object_size)
        self.draw_penguin()
        self.off_grid_movement_direction = "u"
    def move_down(self):
        self.y += self.speed
        fill(0)
        rect(self.x, self.y - self.speed, self.object_size, self.object_size)
        self.draw_penguin()
        self.off_grid_movement_direction = "d"
    def check_in_grid(self):
        self.in_grid = True if int((self.x - self.wall_size) % self.object_size) in range(-1, 2) and int((self.y - self.wall_size - self.game_board_y) % self.object_size) in range(-1, 2) else False
        # after checking, find the precise coordinates and replace the x and y with it. b/c of crazy decimal places that cannot be checked by (self.x - self.wall_size) % self.object_size
