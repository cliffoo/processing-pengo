import random

class Snobee():
    def __init__(self, grid, block_size):
        self.grid = grid
        self.block_size = block_size
        self.start_x = None
        self.start_y = None
        self.end_x = None
        self.end_y = None
        self.path = self.get_path()
    def get_path(self):
        for y in range(len(self.grid)):
            if "s" in self.grid[y]:
                self.start_x = self.grid[y].index("s")
                self.start_y = y
            if "e" in self.grid[y]:
                self.end_x = self.grid[y].index("e")
                self.end_y = y
            found = self.start_x != None and self.end_x != None
            if found:
                break
        x_mag = abs(self.end_x - self.start_x)
        x_dir = 1 if self.end_x - self.start_x >= 0 else -1
        y_mag = abs(self.end_y - self.start_y)
        y_dir = 1 if self.end_y - self.start_y >= 0 else -1
        num_moves = x_mag + y_mag
        moves = [None for i in range(num_moves)]
        rand_order = ["x" for i in range(x_mag)] + ["y" for i in range(y_mag)]
        random.shuffle(rand_order)
        for i in range(num_moves):
            if rand_order[i] == "x":
                moves[i] = ("x", x_dir * self.block_size)
            else:
                moves[i] = ("y", y_dir * self.block_size)
        return moves
    
