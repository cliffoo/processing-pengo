import sys
from Game import game
from Load import *
from Snobee import Snobee
from random import randint, sample
from Penguin import Penguin

def settings():
    global env, screen_width, screen_height
    global num_grids
    env = load_env()
    screen_width = int(env["screen_width"])
    screen_height = int(screen_width / 0.77586206896)
    size(screen_width, screen_height)
    #print(env)
    num_grids = int(env["num_grids"])

def setup():
    global grids
    background(0)
    grids = load_grids(num_grids)
    images = load_images(env)
    game(grids, 4, num_grids, screen_width, screen_height, images)
    #---- Test Snobee ----#
    grid = grids[0][:]
    bee = Snobee(grid, 100)
    #print(bee.path)
    #---------------------#

def game(grids, num_snobees, num_grids, screen_width, screen_height, images):
    global pengo
    clear()
    grid = grids[(randint(0, num_grids - 1))][:]
    grid = put_eggs(grid, num_snobees)
    # ---------- Printing grid ----------
    # for row in grid:
    #     print("  ".join(list(row)))
    # -----------------------------------
    game_board_x = 0
    game_board_y = 0.082536673503995 * screen_height
    game_board_height = 0.8899831905393097 * screen_height
    object_size = 0.0711656847089918 * screen_width
    wall_size = (screen_width - object_size * (len(grid[0]) - 2)) / 2
    blocks_boundaries = setup_blocks_boundaries(grid, object_size, wall_size, game_board_y)
    draw_walls(images, wall_size, game_board_x, game_board_y, game_board_height, screen_width)
    draw_blocks(images, grid, blocks_boundaries, object_size)
    pengo = Penguin(blocks_boundaries, images["diamond"], object_size, 1.3, wall_size, game_board_y)
    pengo.draw_penguin()

def put_eggs(grid, num_snobees):
    blocks_indices = []
    for y in range(1, len(grid) - 1):
        cur_blocks_indices = [(y, i) for i, x in enumerate(grid[y]) if x == "+"]
        blocks_indices.extend(cur_blocks_indices)
    eggs_indices = [blocks_indices[indices] for indices in sample(range(0, len(blocks_indices)), num_snobees)]
    for egg_indices in eggs_indices:
        orig_row = list(grid[egg_indices[0]][:])
        orig_row[egg_indices[1]] = "a"
        new_row = "".join(orig_row)
        grid[egg_indices[0]] = new_row
    return grid

def setup_blocks_boundaries(grid, object_size, wall_size, game_board_y):
    blocks_boundaries = [[] for i in range(len(grid) - 2)]
    for row in range(len(grid) - 2):
        for col in range(len(grid[0]) - 2):
            top_left_x = wall_size + col * object_size
            top_left_y = game_board_y + wall_size + row * object_size
            bottom_right_x = top_left_x + object_size
            bottom_right_y = top_left_y + object_size
            blocks_boundaries[row].append([[top_left_x, top_left_y], [bottom_right_x, bottom_right_y]])
    return blocks_boundaries

def draw_walls(images, wall_size, game_board_x, game_board_y, game_board_height, screen_width):
    image(images["wall_top"], game_board_x, game_board_y, screen_width, wall_size)
    image(images["wall_right"], screen_width - wall_size, game_board_y, wall_size, game_board_height)
    image(images["wall_bottom"], game_board_x, game_board_y + game_board_height - wall_size, screen_width, wall_size)
    image(images["wall_left"], game_board_x, game_board_y, wall_size, game_board_height)

def draw_blocks(images, grid, blocks_boundaries, object_size):
    for row in range(1, len(grid) - 1):
        cells = list(grid[row])
        for cell in range(1, len(cells) - 1):
            if cells[cell] == "+" or cells[cell] == "a":
                image(images["block"], blocks_boundaries[row - 1][cell - 1][0][0], blocks_boundaries[row - 1][cell - 1][0][1], object_size, object_size)
            if cells[cell] == "*":
                image(images["diamond"], blocks_boundaries[row - 1][cell - 1][0][0], blocks_boundaries[row - 1][cell - 1][0][1], object_size, object_size)


def draw():
    if keyPressed:
        pengo.check_in_grid()
        if pengo.in_grid:
            if key == "d":
                pengo.move_right()
            if key == "a":
                pengo.move_left()
            if key == "w":
                pengo.move_up()
            if key == "s":
                pengo.move_down()
    if pengo.off_grid_movement_direction != None:
        if pengo.off_grid_movement_direction == "r":
            pengo.move_right()
        if pengo.off_grid_movement_direction == "l":
            pengo.move_left()
        if pengo.off_grid_movement_direction == "u":
            pengo.move_up()
        if pengo.off_grid_movement_direction == "d":
            pengo.move_down()
        pengo.check_in_grid()
        if pengo.in_grid:
            pengo.off_grid_movement_direction = None
