def load_env():
    try:
        file = open("env.txt", "r")
        fileContent = file.readlines()
        env = {}
        for i in range (len(fileContent)):
            pair = fileContent[i][:-1].split()
            env[pair[0]] = pair[1]
        return env
    except:
        print("Environment file not found. Exiting")
        sys.exit()

def load_grids(num_grids):
    grids = [[] for i in range(num_grids)]
    for i in range(num_grids):
        try:
            file = open("grid_{}.txt".format(str(i)), "r")
            fileContent = file.readlines()
            grid = []
            for j in range(len(fileContent)):
                grid.append(fileContent[j][:-1])
            grids[i] = grid
        except:
            print("File grid_{}.txt not found. Exiting".format(str(i)))
            sys.exit()
    return grids

def load_images(env):
    images = {}
    images["block"] = loadImage(env["block_image"])
    images["diamond"] = loadImage(env["diamond_image"])
    images["wall_top"] = loadImage(env["wall_top_image"])
    images["wall_right"] = loadImage(env["wall_right_image"])
    images["wall_bottom"] = loadImage(env["wall_bottom_image"])
    images["wall_left"] = loadImage(env["wall_left_image"])
    return images
